import unittest
import os
from barrel import Database
from threading import Thread


class ListeningThread(Thread):

    """This thread updates a document 3 times"""

    result = 0

    def __init__(self, database_url, docid):
        Thread.__init__(self)
        self.database_url = database_url
        self.docid = docid

    def run(self):
        database = Database(self.database_url)

        result = []
        i = 0
        for doc in database.changes():
            result.append(doc["count"])
            i = i + 1
            if(i >= 3):
                break

        self.result = result


class TestBarrel(unittest.TestCase):

    database_url = 'http://localhost:7080/dbs/source'

    def setUp(self):
        if "BARREL_URL" in os.environ:
            barrel_url = os.environ['BARREL_URL']
            self.database_url = barrel_url + '/dbs/source'

    def test_connect_to_database(self):
        db = Database(self.database_url)
        self.assertTrue(isinstance(db, Database))


    def test_changes_stream(self):
        docid = "testpython"
        listeningThread = ListeningThread(self.database_url, docid)
        listeningThread.start()

        db = Database(self.database_url)
        doc = { 'id': docid, 'count': 0}
        db.put(doc)

        for i in range(1, 4):
            doc = db.get(docid)
            doc['count'] = i
            db.put(doc)

        listeningThread.join()
        self.assertEqual([1,2,3], listeningThread.result)
        doc = db.get(docid)
        db.delete(docid, doc['_rev'])


if __name__ == '__main__':
    unittest.main()
