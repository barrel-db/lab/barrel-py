#!/usr/bin/env python

from setuptools import setup, find_packages

from sseclient.version import name, version

with open('README.rst') as readme:
    long_description = readme.read()

setup(
    name=name,
    version=version,
    author='Bernard Notarianni',
    author_email='bernardn@barrel-db.org',
    description='Barrel-db client for Python',
    license='Apache Software License v2',
    long_description=long_description,
    zip_safe=True,
    packages=find_packages(),
    classifiers=[
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
    url='https://gitlab.com/barrel-db/barrel-py',
)
