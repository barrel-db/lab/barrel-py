Barrel-db client for Python
===========================

A Python client for Barrel-db. Provide methods to store and retrieve documents
and stream updated documents to create reactive micro-services.

Installation
------------

.. code::

    $ pip install barrel-py

Usage
-----

.. code:: python

   from barrel import Database
   from threading import Thread

   database_url = 'http://localhost:8080/source'
   docid = 'chat'

   database = Database(database_url)


   def read_doc():
   doc = database.get(docid)
   if doc is None:
       return {'id': 'chat', 'message': ''}
   return doc


   class Sender(Thread):

       """This thread sends your message to the server"""

       def __init__(self):
          Thread.__init__(self)

       def run(self):
          while True:
             message = raw_input('Your message:')
             doc = read_doc()
             doc['message'] = message
             database.put(doc)


   class Receiver(Thread):

       """This thread receives and print messages from the server"""

       def __init__(self):
          Thread.__init__(self)

       def run(self):

           for doc in database.changes(docid):
           print(doc['message'])


   thread1 = Sender()
   thread2 = Receiver()

   thread1.start()
   thread2.start()

